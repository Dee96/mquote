package com.example.deepak.mquote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuthException;

public class WelcomeActivity extends AppCompatActivity {


    SignInButton button;
    FirebaseAuthException mAuth;
    private final int RC_SIGN_IN= 1;
    GoogleApiClient  mGoogleapiClient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

       button =(SignInButton)findViewById(R.id.gbut);
//        mGoogleapiClient = new GoogleApiClient.builder(this)
//        {
//            build();
//
//        }

    }


    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build();

    private void signIn()
    {
        Intent signInIntent = mAuth.mGoogleSignInClient.getSignInIntent(mGoogleapiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

   

}

