package com.example.deepak.mquote;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;  //splash screen will be shown for 2 seconds

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               Intent mainIntent = new Intent(MainActivity.this, com.example.deepak.mquote.WelcomeActivity.class);
               startActivity(mainIntent);
               finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
